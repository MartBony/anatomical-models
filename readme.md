# Anatomical models
Run freesurfrer and fastsurfer pipelines.

## Requirements
This module requires to install the CLI modules from this repo in slicer : https://gitlab.com/MartBony/cli-modules-stim.
It also uses functions from the EPISTIM module. You should link the EPISTIM module in the folder through the following command :
```bash
cd path/to/anatomical/models/folder
ln -s path/to/epistim/folder/on/your/computer ./epiSTIM
```