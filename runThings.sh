docker run -it --user $(id -u):$(id -g) \
    -v /home/martin.gregorio/Documents/epiloc/analyses/anat/patients/sub_fasts/temp_FastSurfer/output/mri:/data \
    -v /home/martin.gregorio/Apps/Slicer_modules/ModulesSTIM/FastSurfer/modules/fs_license:/usr/local/freesurfer/ \
    freesurfer/freesurfer:7.4.1 \
    mri_label2vol --seg \
    /data/aseg.mgz \
    --temp /data/rawavg.mgz \
    --o /data/aseg-in-rawavg.nii.gz \
    --regheader /data/aseg.mgz

docker run --rm --user $(id -u):$(id -g) -v /home/martin.gregorio/Documents/epiloc/analyses/anat/patients/sub_fasts/temp_FastSurfer/output/mri:/data freesurfer/freesurfer:7.4.1 mri_label2vol --seg /data/aparc+aseg.mgz --temp /data/rawavg.mgz --o /data/aparc+aseg-in-rawavg.nii.gz --regheader /data/aparc+aseg.mgz
docker run --rm --user $(id -u):$(id -g) -v /home/martin.gregorio/Documents/epiloc/analyses/anat/patients/sub_fasts/temp_FastSurfer/output/mri:/data freesurfer/freesurfer:7.4.1 mri_label2vol --seg /data/aparc.a2009s+aseg.mgz --temp /data/rawavg.mgz --o /data/aparc.a2009s+aseg-in-rawavg.nii.gz --regheader /data/aparc.a2009s+aseg.mgz
/home/martin.gregorio