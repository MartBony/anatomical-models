import os
from typing import Annotated, Optional
import time

import vtk
import qt
import slicer
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin
from slicer.parameterNodeWrapper import parameterNodeWrapper



from importlib import reload
import sys
from modules.FsurferPipeline import FsurferPipeline
from modules.AtlasYEBPipeline import AtlasYEBPipeline



import shutil




moduleDir = os.path.dirname(__file__)



#
# AnatomicalModels
#


class AnatomicalModels(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """


    def __init__(self, parent):
        ScriptedLoadableModule.__init__(self, parent)
        self.parent.title = ("Anatomical Models")
        self.parent.categories = ["Epimods"]
        self.parent.dependencies = []
        self.parent.contributors = ["Martin Gregorio"]
        self.parent.helpText = ("AnatomicalModels module for STIM.")
        self.parent.acknowledgementText = ("This file was originally developed by Martin Gregorio and wraps some epiSTIM scripts written by Sara FERNANDEZ VIDAL")































#
# AnatomicalModelsParameterNode
#


@parameterNodeWrapper
class AnatomicalModelsParameterNode:
    """
    The parameters needed by module.

    fsLicensePath - Path to the freesurfer license
    """

    fsLicensePath: str




class AnatomicalModelsWidget(ScriptedLoadableModuleWidget, VTKObservationMixin):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """


    def __init__(self, parent=None):
        """
        Called when the user opens the module the first time and the widget is initialized.
        """
        ScriptedLoadableModuleWidget.__init__(self, parent)
        VTKObservationMixin.__init__(self)
        self.logic = None

        self._parameterNode = None
        self._parameterNodeGuiTag = None

        self.historyDir = os.path.join(moduleDir, 'History')
        self.historyPath = os.path.join(self.historyDir, 'history.txt')

    
        self.runningFast = False
        self.runningFree = False
        self.runningPrep = False
        self.runningAtlas = False

        self.reconAllFastCLINode = None
        self.reconAllFreeCLINode = None
        self.sMRIprepCLINode = None

        self._enabled = False

        self.timeStart = 0
        self.timeTracker = {}

        self.pbars = {
            "freesurfer": None,
            "fastsurfer": None
        }

        self.cliNodes = {
            "fastsurfer": None,
            "freesurfer": None
        }

        self.yebAtlasLUTPath = None
        self.yebAtlasTerminologyPath = None



    def setup(self):
        """
        Called when the user opens the module the first time and the widget is initialized.
        """
        ScriptedLoadableModuleWidget.setup(self)


        uiWidget = slicer.util.loadUI(self.resourcePath('UI/AnatomicalModels.ui'))
        self.layout.addWidget(uiWidget)
        self.ui = slicer.util.childWidgetVariables(uiWidget)
        self.observersAdded = False

     
    
        uiWidget.setMRMLScene(slicer.mrmlScene)


        atlasYebPaths = {
            "LUTPath": self.resourcePath('LUT/fantomas_color_LUT.ctbl'),
            "terminologyPath": self.resourcePath('Terminology/AtlasYEB_NucleiArg.xml')
        }

        self.logic = AnatomicalModelsLogic(atlasYebPaths)





        self.ui.fs_license_button.clicked.connect(self.requestLicense)
        self.ui.run_fastsurfer_button.clicked.connect(self.initFastPipeline)
        self.ui.run_freesurfer_button.clicked.connect(self.initFreesPipeline)
        self.ui.run_atlasyeb_button.clicked.connect(self.initAtlasYEBPipeline)


        self.ui.info_label.hide()
        # self.ui.run_smriprep_button.clicked.connect(self.runsMRIprep)

        self.grabAndShowT1()


        # These connections ensure that we update parameter node when scene is closed
        self.addObserver(slicer.mrmlScene, slicer.mrmlScene.StartCloseEvent, self.onSceneStartClose)
        self.addObserver(slicer.mrmlScene, slicer.mrmlScene.EndCloseEvent, self.onSceneEndClose)
        # Make sure parameter node is initialized (needed for module reload)
        self.initializeParameterNode()


    def cleanup(self) -> None:
        """Called when the application closes and the module widget is destroyed."""
        self.removeObservers()

    def enter(self):
        self.grabAndShowT1()
        # Make sure parameter node exists and observed
        self.initializeParameterNode()

    def onReload(self):
        """
        Override reload scripted module widget representation to reload dependencies.
        """

        reload(sys.modules["modules.FsurferPipeline"])
        reload(sys.modules["modules.AtlasYEBPipeline"])
        from modules.FsurferPipeline import FsurferPipeline
        from modules.AtlasYEBPipeline import AtlasYEBPipeline

        if isinstance(self, ScriptedLoadableModuleWidget):
            ScriptedLoadableModuleWidget.onReload(self)

    def exit(self):
        # Do not react to parameter node changes (GUI will be updated when the user enters into the module)
        if self._parameterNode:
            self._parameterNode.disconnectGui(self._parameterNodeGuiTag)
            self._parameterNodeGuiTag = None
            self.removeObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent, self._checkCanApply)
        
        

    def onSceneStartClose(self, caller, event) -> None:
        """Called just before the scene is closed."""
        # Parameter node will be reset, do not use it anymore
        self.setParameterNode(None)
        self.stopProcessing("fastsurfer")
        self.stopProcessing("freesurfer")

    def onSceneEndClose(self, caller, event) -> None:
        """Called just after the scene is closed."""
        # If this module is shown while the scene is closed then recreate a new parameter node immediately
        if self.parent.isEntered:
            self.initializeParameterNode()

    
    def initializeParameterNode(self) -> None:
        """Ensure parameter node exists and observed."""
        # Parameter node stores all user choices in parameter values, node selections, etc.
        # so that when the scene is saved and reloaded, these settings are restored.
        self.setParameterNode(self.logic.getParameterNode())
        self.loadHistory()


    def setParameterNode(self, inputParameterNode: Optional[AnatomicalModelsParameterNode]) -> None:
        """
        Set and observe parameter node.
        Observation is needed because when the parameter node is changed then the GUI must be updated immediately.
        """

        if self._parameterNode:
            self._parameterNode.disconnectGui(self._parameterNodeGuiTag)
            self.removeObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent, self._checkCanApply)
        self._parameterNode = inputParameterNode
        if self._parameterNode:
            # Note: in the .ui file, a Qt dynamic property called "SlicerParameterName" is set on each
            # ui element that needs connection.
            self._parameterNodeGuiTag = self._parameterNode.connectGui(self.ui)
            self.addObserver(self._parameterNode, vtk.vtkCommand.ModifiedEvent, self._checkCanApply)
            self._checkCanApply()
            

    def _checkCanApply(self, caller=None, event=None) -> None:
        if self._parameterNode and self._parameterNode.fsLicensePath:
            self.ui.label_seq.toolTip = "Compute output volume"
            #self.ui.label_seq.enabled = True
        else:
            self.ui.label_seq.toolTip = "Select input and output volume nodes"
            #self.ui.label_seq.enabled = False

    def saveHistory(self, fsLicenseDir): # Save input patient to history
        if not os.path.exists(self.historyDir):
            os.mkdir(self.historyDir)
        with open(self.historyPath, 'w') as f:
            f.write(fsLicenseDir)


    def loadHistory(self): # Get Back former patient
        if os.path.exists(self.historyPath):
            with open(self.historyPath, 'r') as f:
                self.ui.fs_license_input.setText(f.readline().rstrip("\n"))
    
    def requestLicense(self):
        wd = self.ui.fs_license_input.text
        
        dirName = qt.QFileDialog.getOpenFileName(None, "Select Freesurfer License", wd)
        if dirName:
            self.ui.fs_license_input.setText(dirName)
            self.saveHistory(dirName)

    @property
    def enabled(self):
        return self._enabled
    
    @enabled.setter
    def enabled(self, state):
        self.ui.run_fastsurfer_button.enabled = state
        self.ui.run_freesurfer_button.enabled = state
        self.ui.run_atlasyeb_button.enabled = state
        self.ui.run_smriprep_button.enabled = False
        
    
    def grabAndShowT1(self):
        try:
            self.logic.importNodes(slicer.modules.DataManagementWidget.logic.subject.niftiNodeIds)
        except AttributeError as err:
            self.printWarn(err)

        t1Node = self.logic.getMRINode() # TODO : detect path not node

        self.enabled = t1Node is not None

        if(t1Node is not None):
            self.ui.label_seq.text = f"Sequence detected (Filename : {t1Node.GetName()})"

            # Show in view
            slicer.util.setSliceViewerLayers(background=t1Node, foregroundOpacity=0)
            slicer.util.resetSliceViews()
           
            return

        else:   
            self.ui.label_seq.text = "No t1 mri sequence detected, please import a t1 sequence using the Data Management module"


    def initFastPipeline(self):
        
        if(not self.runningFast):
            self.runningFast = True
            fsLicenseDir = self.ui.fs_license_input.text
        
            print("Starting")

            self.timeStart = time.time()

            self.logic.setupFSPipeline(fsLicenseDir, "temp_FastSurfer", mode = "fastsurfer")
            cliNodeDownload = self.logic.fastsPipeline.cliNodeDownload
            if(not cliNodeDownload):
                self.runFS("fastsurfer")
            else:
                self.ui.info_label.show()
                self.ui.info_label.text = "Downloading resources, processing will begin shortly ..."
                self.enabled = False
                cliNodeDownload.AddObserver('ModifiedEvent', lambda *argv: self.waitCLI(*argv, "fastsurfer"))
    

    def initFreesPipeline(self):
        if(not self.runningFree):
            self.runningFree = True
            fsLicenseDir = self.ui.fs_license_input.text
        
            print("Starting")
            self.timeStart = time.time()
            self.timeTracker = {}

            self.logic.setupFSPipeline(fsLicenseDir, "temp_FreeSurfer", mode = "freesurfer")
            cliNodeDownload = self.logic.freesPipeline.cliNodeDownload
            if(not cliNodeDownload):
                self.runFS("freesurfer")
            else:
                self.ui.info_label.show()
                self.ui.info_label.text = "Downloading resources, processing will begin shortly ..."
                self.enabled = False
                cliNodeDownload.AddObserver('ModifiedEvent', lambda *argv: self.waitCLI(*argv, "freesurfer"))


    def runFS(self, mode):
        try:
            with slicer.util.WaitCursor():
                self.setupProgressBar(mode)

                if(mode == "fastsurfer"):
                    self.cliNodes["fastsurfer"] = self.logic.runReconAllFast()
                elif(mode == "freesurfer"):
                    self.cliNodes["freesurfer"] = self.logic.runReconAllFree()

                self.pbars[mode].canceled.connect(lambda: self.stopProcessing(mode))
                self.cliNodes[mode].AddObserver('ModifiedEvent', lambda caller, event: self.updateProgress(caller, mode))
                

        except Exception as exc:
            if(hasattr(exc, "__traceback__")):
                import traceback

                # `e` is an exception object that you get from somewhere
                traceback_str = ''.join(traceback.format_tb(exc.__traceback__))
                print("Traceback : ", traceback_str)
            print("Err : ", exc)
    
    def waitCLI(self, cliNode, event, mode):
        if cliNode.GetStatus() & cliNode.Completed:
            if cliNode.GetStatus() & cliNode.ErrorsMask:
                # error
                errorText = cliNode.GetErrorText()
                print("CLI execution failed: " + errorText)
            else:
                # success
                print("CLI execution succeeded. Output model node ID: "+cliNode.GetParameterAsString("OutputGeometry"))
                self.ui.info_label.text = ""
                self.ui.info_label.hide()
                self.enabled = True
                self.runFS(mode)
    
    def setupProgressBar(self, mode):
        if(not self.pbars[mode]):
            self.pbars[mode] = slicer.util.createProgressDialog(value=0, maximum=100, autoClose=False, autoReset=False)
        
        self.pbars[mode].labelText = "Downloading additional resources"
        self.pbars[mode].open()
        self.pbars[mode].windowTitle = f"Running {mode} pipeline"
        self.pbars[mode].value = 0

    def updateProgress(self, caller, mode):
        progress = int(caller.GetProgress())
        if(self.pbars[mode] and progress > 0):
            self.pbars[mode].labelText = "Running recon-all"
            self.pbars[mode].value = progress * 0.98
            if(progress-1 not in self.timeTracker):
                self.timeTracker[progress-1] = int(time.time() - self.timeStart)

        if caller.GetStatus() & caller.Completed:
            if caller.GetStatus() & caller.ErrorsMask:
                errorText = caller.GetErrorText()
                print("CLI execution failed: " + errorText)
            else:
                
                self.pbars[mode].labelText = "Importing into the project, slicer may freeze for a while"
                slicer.app.processEvents()
                time.sleep(1)
                self.logic.runConverts(mode)

            self.stopProcessing(mode, message = f"{mode} finished with success")


    def stopProcessing(self, mode, message = "The process was interrupted"):
        if(self.cliNodes[mode]):
            self.cliNodes[mode].Cancel()
        if(self.pbars[mode]):
            self.pbars[mode].canceled.disconnect()
            self.pbars[mode].close()


        self.timeTracker[100] = time.time() - self.timeStart
        print("Time Tracker :",self.timeTracker)
        
        if(self.runningFast):
            slicer.util.delayDisplay(message, -1)


        with open(os.path.join(moduleDir,'Testing/TimetrackFS.py'),'a') as file:
            file.write(f"# {mode} \n")
            file.write("dict = { \n")
            for k in sorted (self.timeTracker.keys()):
                file.write("\t'%s':'%s', \n" % (k, self.timeTracker[k]))
            file.write("} \n \n")

        self.timeTracker = {}

        
        self.cliNodes[mode].RemoveAllObservers()

        self.runningFast = False

    def initAtlasYEBPipeline(self):
        if(not self.runningAtlas):
            self.runningAtlas = True
    
            with slicer.util.tryWithErrorDisplay("Failed to run AtlasYEB", waitCursor=True):

                """ if not self.ui.segmentationSelector_LH.currentNode():
                    self.ui.segmentationSelector_LH.addNode()

                if not self.ui.segmentationSelector_RH.currentNode():
                    self.ui.segmentationSelector_RH.addNode() """

                # Compute output
                self.logic.runAtlasYEB()
            

                # self.ui.segmentsTableView_LH.setSegmentationNode(self.ui.segmentationSelector_LH.currentNode())
                # self.ui.segmentsTableView_RH.setSegmentationNode(self.ui.segmentationSelector_RH.currentNode())



    """
    def runsMRIprep(self):
        try:
            if(not self.runningPrep):
                self.runningPrep = True
                with slicer.util.WaitCursor():
                    self.setupPrepProgressBar()

                    self.sMRIprepCLINode = self.logic.runsMRIprep()
                    self.sMRIprepCLINode.AddObserver('ModifiedEvent', self.updatePrepProgress)
                    self.freePbar.canceled.connect(self.stopPrepProcessing)
                    
        except Exception as exc:
            if(hasattr(exc, "__traceback__")):
                import traceback

                # `e` is an exception object that you get from somewhere
                traceback_str = ''.join(traceback.format_tb(exc.__traceback__))
                print("Traceback : ", traceback_str)
            print("Err : ", exc)

    def setupPrepProgressBar(self, title = 'Running sMRIprep ...'):
        if(self.prepPbar):
            self.prepPbar.close()
        self.prepPbar = slicer.util.createProgressDialog(value=0, maximum=100, windowTitle=title, autoClose=False)

    def updatePrepProgress(self, caller, event):
        if(self.prepPbar):
            self.prepPbar.value = int(caller.GetProgress())

        if caller.GetStatus() & caller.Completed:
            # TODO : Implement errors or success of cli node
            if caller.GetStatus() & caller.ErrorsMask:
                errorText = caller.GetErrorText()
                print("CLI execution failed: " + errorText)

            self.stopPrepProcessing()


    def stopPrepProcessing(self, *args):
        if(self.sMRIprepCLINode):
            self.sMRIprepCLINode.Cancel()
        if(self.prepPbar):
            self.prepPbar.close()

        self.runningPrep = False """




    def printWarn(self, msg):
        print("FastSurfer module prints:", msg)
   
        

  




    def updateEnableState(self):

        enabled = bool(self.logic.inputVolumeNode)

   














































#
# AnatomicalModelsLogic
#

class AnatomicalModelsLogic(ScriptedLoadableModuleLogic):

    """
    Implement the logic to calculate record fiduciels volume
    """
    MRINodeId = None

    atlasYEBPipeline = None
    # yebAtlasTerminologyPath=None


    def __init__(self, atlasYebPaths):

        self.observerTags = {}
        self.outputAPCFilePath = None
        self.translatedCoordsOrbitesPoints = {}

        self._pipelines = {
            "freesurfer" : None,
            "fastsurfer" : None
        }

        self.sMRIprepOutput = "temp_smriprep"

        DMLogic = slicer.modules.DataManagementWidget.logic
        self.atlasYEBPipeline = AtlasYEBPipeline(DMLogic.subject.folderName, **atlasYebPaths)


        ScriptedLoadableModuleLogic.__init__(self)
    
    def getParameterNode(self):
        return AnatomicalModelsParameterNode(super().getParameterNode())

    @property
    def fastsPipeline(self):
        return self._pipelines["fastsurfer"]

    @property
    def freesPipeline(self):
        return self._pipelines["freesurfer"]




    def importNodes(self, niftiIdsDico):
        self.MRINodeId = None

        if("t1mri" in niftiIdsDico):
            self.MRINodeId = niftiIdsDico["t1mri"]
            if(self.getMRINode()):
                self.getMRINode().GetDisplayNode().SetVisibility2D(True)

    
    def getMRINode(self):
        if(self.MRINodeId):
            return slicer.mrmlScene.GetNodeByID(self.MRINodeId)
        return None


    def setupFSPipeline(self, fsLicenseDir, location, mode = "fastsurfer"):
        DMLogic = slicer.modules.DataManagementWidget.logic

        if(
            not DMLogic.subject.t1mriPath 
            or not os.path.exists(DMLogic.subject.t1mriPath) 
            or not os.path.exists(fsLicenseDir)
            ):
            raise Exception("Incorrect input")
            
                
        self._pipelines[mode] = FsurferPipeline(
            t1Path        = DMLogic.subject.t1mriPath,
            tempDir       = os.path.join(DMLogic.subject.path, location),
            fsLicensePath = fsLicenseDir,
            finalSegDir   = DMLogic.subject.segmentationsMriRefDir,
            finalMeshDir  = DMLogic.subject.meshMriRefDir,
            subfolder     = mode)
   

      

    def runReconAllFast(self):

        if(not hasattr(slicer.modules, "reconallfast")):
            slicer.util.errorDisplay("This action requires to install the recon all fast CLI module")
            raise Exception("Recon all fast CLI module not found")

        self.initTemporaryDir(self.fastsPipeline.tempDir)

        param = {
            "t1ImagePath": self.fastsPipeline.t1Path, 
            "outputDir": self.fastsPipeline.tempDir, 
            "subjectName": self.fastsPipeline.outputSubjectName, 
            "fsLicensePath": self.fastsPipeline.fsLicensePath
        }


        cliNode = slicer.cli.run(slicer.modules.reconallfast, parameters=param)
        


        return cliNode

    def runReconAllFree(self):

        if(not hasattr(slicer.modules, "reconallfree")):
            slicer.util.errorDisplay("This action requires to install the recon all free CLI module")
            raise Exception("Recon all free CLI module not found")

        self.initTemporaryDir(self.freesPipeline.tempDir)

        param = {
            "t1ImagePath": self.freesPipeline.t1Path, 
            "outputDir": self.freesPipeline.tempDir, 
            "subjectName": self.freesPipeline.outputSubjectName, 
            "fsLicensePath": self.freesPipeline.fsLicensePath
        }


        cliNode =  slicer.cli.run(slicer.modules.reconallfree, parameters=param)
        


        return cliNode



    """ def runsMRIprep(self):
        if(not hasattr(slicer.modules, "smriprep")):
            slicer.util.errorDisplay("This action requires to install the smriprep CLI module")
            raise Exception("Smriprep CLI module not found")
        
        self.initTemporaryDir(self.sMRIprepOutput)

        param = {
            "output": self.freesPipeline.t1Path,
            "fsLicensePath": self.fastsPipeline.fsLicensePath
        }

        cliNode =  slicer.cli.run(slicer.modules.reconallfree, parameters=param) """


    def initTemporaryDir(self, directory):
        if(os.path.isdir(directory)):
            shutil.rmtree(directory) # Remove old temporary data
        os.mkdir(directory)

    def runConverts(self, mode):
        try:
            pipeline = self._pipelines[mode]
            DMLogic = slicer.modules.DataManagementWidget.logic
            segNodes = pipeline.runConverts()

            DMLogic.registerSegmentationNodes(segNodes, pipeline.subfolder)

        except ValueError as err:
            print("FastSurfer module : Converting fastsurfer output failed",  err)
            pass

    def runAtlasYEB(self):
        self.atlasYEBPipeline.runAtlasYEB(self.getMRINode())