from epiSTIM.utils.Logger import Logger
import slicer
import os
import subprocess
import nibabel
import shutil
import numpy as np
import epiSTIM.core.TransformUtils as tu
from epiSTIM.core.AffineMatrix import AffineMatrix
from epiSTIM.core.PathUtils import ensureDir
import vtkSegmentationCorePython as vtkSegmentationCore
import time



moduleDir = os.path.dirname(__file__)


class FsurferPipeline:
    def __init__(self, t1Path, tempDir, fsLicensePath, finalSegDir, finalMeshDir, subfolder):

        self.cliNodeDownload = None
        self.t1Path = t1Path

        self.subfolder = subfolder

        self.fsLicensePath = fsLicensePath



        self.tempDir = tempDir
        self.outputSubjectName = "output"
        self.tempOutputDir = os.path.join(self.tempDir, self.outputSubjectName)
        self.finalSegDir = os.path.join(finalSegDir, subfolder)
        self.finalMeshDir = os.path.join(finalMeshDir, subfolder)


        self.subjectMriDir = os.path.join(self.tempOutputDir,'mri')
        self.subjectSurfDir = os.path.join(self.tempOutputDir,'surf')
        self.subjectLabelDir = os.path.join(self.tempOutputDir,'label')


        self.asegMaskPath = os.path.join(self.subjectMriDir,'aseg.mgz')
        self.apark2009asegPath = os.path.join(self.subjectMriDir,'aparc.a2009s+aseg.mgz')
        self.aparcasegPath = os.path.join(self.subjectMriDir,'aparc+aseg.mgz')

        self.volRefTemplatePath = os.path.join(self.subjectMriDir,'T1.mgz')

        self.asegTemplatePath = os.path.join(self.subjectMriDir,'rawavg.mgz')
        self.asegMaskNativeSpacePath = os.path.join(self.subjectMriDir,'aseg-in-rawavg.nii.gz')
        self.apark2009asegNativeSpacePath = os.path.join(self.subjectMriDir,'aparc.a2009s+aseg-in-rawavg.nii.gz')
        self.aparcasegNativeSpacePath = os.path.join(self.subjectMriDir,'aparc+aseg-in-rawavg.nii.gz')

        self.segs = {
            'FS_Destrieux_Segmentation' : ['aparc.a2009s+aseg.nii.gz', 'FS_Destrieux_Segmentation.seg.vtm'],
            'FS_Desikan_Segmentation'   : ['aparc+aseg.nii.gz', 'FS_Desikan_Segmentation.seg.vtm'],
            'FS_Aseg_Segmentation'      : ['aparc.a2009s+aseg.nii.gz', 'FS_Aseg_Segmentation.seg.vtm']
            }

     
          
          

        # Import Dockers

        self.customFSDockerImage = "licensedfreesurfer"


        # Check if image already exists
        try:
            checkOut = subprocess.check_output(
                ["docker", "image", "inspect", self.customFSDockerImage],
                text=True,
                encoding="utf-8"
                ) # Will fail if image deosn't exist
            

        except Exception as exc:

            print("Building the image for FS Pipeline")

            # Build image with fs license based on freesurfer
            freeSurfDockerImage = "freesurfer/freesurfer:7.4.1"
            localLicensePath = os.path.join(moduleDir, ".license")
            shutil.copy(self.fsLicensePath, localLicensePath)
            with open(os.path.join(moduleDir, "Dockerfile"), 'w') as dockerfile:
                dockerfile.write(f"FROM {freeSurfDockerImage}")
                dockerfile.write("\n")
                dockerfile.write(f"COPY .license /usr/local/freesurfer/.license")

            slicer.util.delayDisplay(
                "Downloading the necessary ressources, this may take a while", 
                3000
                )

            # Invoke CLI module
            if(not hasattr(slicer.modules, "backgroundprocess")):
                slicer.util.errorDisplay("This action requires to install the backgroundprocess CLI module")
                raise Exception("backgroundprocess CLI module not found")

            param = {
                "command": f"docker build -t {self.customFSDockerImage} {moduleDir}"
            }

            self.cliNodeDownload = slicer.cli.run(slicer.modules.backgroundprocess, parameters=param)



    def Label2vol(self, seg_file, template_file, vol_label_file, reg_header):
        # See https://nipype.readthedocs.io/en/latest/api/generated/nipype.interfaces.freesurfer.model.html#label2vol
        workDir = os.path.dirname(seg_file)
        
        seg_file = os.path.basename(seg_file)
        template_file = os.path.basename(template_file)
        vol_label_file = os.path.basename(vol_label_file)
        reg_header = os.path.basename(reg_header)
        
        docker_command = [
            "docker", "run", "--rm",
            "--user", f"{os.getuid()}:{os.getgid()}",
            "-v", f"{workDir}:/data",
            self.customFSDockerImage,
            "mri_label2vol", 
            "--seg", os.path.join("/data/", seg_file), 
            "--temp", os.path.join("/data/", template_file),
            "--o", os.path.join("/data/", vol_label_file),
            "--regheader", os.path.join("/data/", reg_header)
        ]


        subprocess.run(docker_command, capture_output=True, encoding='utf-8')

      

    def convertAsegToNative(self):
        """
        Converts the segmentation file from freesurfer space to native space
        """

        if not os.path.exists(self.asegMaskNativeSpacePath):
            if os.path.exists(self.asegMaskPath):
                self.Label2vol(
                    seg_file = self.asegMaskPath,
                    template_file = self.asegTemplatePath,
                    vol_label_file = self.asegMaskNativeSpacePath,
                    reg_header = self.asegMaskPath
                    )
                
            else:
                raise Exception("Freesurfer segmentation file not found: %s" % self.asegMaskPath)

    def convertAparcPlusAsegToNative(self):
        """
        Converts the segmentation file from freesurfer space to native space
        """
        if not os.path.exists(self.aparcasegNativeSpacePath):
            if os.path.exists(self.aparcasegPath):
                self.Label2vol(
                    seg_file = self.aparcasegPath,
                    template_file = self.asegTemplatePath,
                    vol_label_file = self.aparcasegNativeSpacePath,
                    reg_header = self.aparcasegPath
                    )
                
            else:
                raise Exception("Freesurfer segmentation file not found: %s" % self.aparcasegPath)

    def convertAparc2009PlusASegToNative(self):
        """
        Converts the segmentation file from freesurfer space to native space
        """
        if not os.path.exists(self.apark2009asegNativeSpacePath):
            if os.path.exists(self.apark2009asegPath):
                self.Label2vol(
                    seg_file = self.apark2009asegPath,
                    template_file = self.asegTemplatePath,
                    vol_label_file = self.apark2009asegNativeSpacePath,
                    reg_header = self.apark2009asegPath
                    )
            
            else:
                raise Exception("Freesurfer segmentation file not found: %s" % self.apark2009asegPath)

    def exportPialAndWhiteFromFsSurf(self):
        """
        Extracts a VTK mesh in ITK space of the brain ventricles using freesurfer segmentation
        """

        # make sure that the segmentation was converted to native space
        surfNames = ['lh.pial', 'lh.white', 'rh.pial', 'rh.white']

        self.convertFsSurfsToVTK(surfNames)


        ima = nibabel.load(self.asegMaskNativeSpacePath)
        data = ima.get_fdata()
        shape = np.array(data.shape)

        vc = shape/2.0
        qform = ima.affine
        vcXYZ = tu.applyTransformToPoint(vc, qform)
        # translation for mesh
        m = np.eye(4, 4)
        m[0:3, 3] = vcXYZ
        print(m)
        affine = AffineMatrix()
        affine.matrix[0:3, 3] = vcXYZ
        print(affine.matrix)
        affine.inverse()
        print(affine.matrix)
        fileNameMat = os.path.join(
            self.finalMeshDir, 'natifToFreeSurferRef.xfm')
        affine.writeToXFMFile(fileNameMat)
        print(fileNameMat)
        for surfName in surfNames:
            surfFS_Path = os.path.join(
                self.subjectSurfDir, surfName+'_converted.vtk')
            if os.path.exists(surfFS_Path):
                print(self.asegMaskNativeSpacePath)
                surfDest_Path = os.path.join(
                    self.finalMeshDir, surfName.replace(".", "_")+'.vtk')
                print(surfFS_Path)
                print(surfDest_Path)
                print(m)
                mu.transformVTKMesh(surfFS_Path, surfDest_Path, m)
        self.convertVTKToASCFsSurfs(surfNames,self.finalMeshDir)



    def exportFreesurferAsegLabelMap(self):
        """
        Copy asegMaskNativeSpacePath to Ouput self.finalSegDir
        """
    
        if os.path.exists(self.asegMaskNativeSpacePath):
            dest = os.path.join(self.finalSegDir,'aseg.nii.gz')
            ensureDir(dest)
            shutil.copyfile(self.asegMaskNativeSpacePath, dest)
    

    def exportFreesurferAparcPlusASegLabelMap(self):
        """
        Copy asegMaskNativeSpacePath to Ouput self.finalSegDir
        """
        
        # make sure that the segmentation was converted to native space
        print(self.finalSegDir)
        print(self.aparcasegNativeSpacePath)
        if os.path.exists(self.aparcasegNativeSpacePath):
            dest = os.path.join(self.finalSegDir,'aparc+aseg.nii.gz')
            ensureDir(dest)
            print(dest)
            shutil.copyfile(self.aparcasegNativeSpacePath,dest)
            
 
    
    def exportFreesurferAparc2009PlusASegLabelMap(self):
        """
        Copy asegMaskNativeSpacePath to Ouput self.finalSegDir
        """
        
        # make sure that the segmentation was converted to native space

        if os.path.exists(self.apark2009asegNativeSpacePath):
            dest = os.path.join(self.finalSegDir,'aparc.a2009s+aseg.nii.gz')
            ensureDir(dest)
            shutil.copyfile(self.apark2009asegNativeSpacePath,dest)

    
                
    def MRIsConvert(self, in_file, out_file, out_datatype):
        # See https://nipype.readthedocs.io/en/latest/api/generated/nipype.interfaces.freesurfer.preprocess.html#mriconvert
        docker_command = [
            "docker", "run", "--rm",
            "--user", f"{os.getuid()}:{os.getgid()}",
            "-v", f"{os.path.dirname(in_file)}:/data",
            self.customFSDockerImage,
            "mris_convert", 
            "--input_volume", os.path.join("/data/", os.path.basename(in_file)), 
            "--output_volume", os.path.join("/data/", os.path.basename(out_file)), 
            "--out_data_type", out_datatype
        ]

        subprocess.run(docker_command, capture_output=True)
      


    def convertFsSurfsToVTK(self, surfNames):
        """
        Converts the segmentation file from freesurfer space to native space
        """
        pwd = os.getcwd()
        print(pwd)
        for surfName in surfNames:

            pathSurfFreeSurfer = os.path.join(self.subjectSurfDir, surfName)

            if os.path.exists(pathSurfFreeSurfer):
                pathConvertedFile = os.path.join(self.subjectSurfDir, surfName+'_converted.vtk')

                self.MRIsConvert(in_file=pathSurfFreeSurfer, out_file=pathConvertedFile, out_datatype='vtk')
    

    def convertVTKToASCFsSurfs(self, surfNames,dir):
        """
        Converts the segmentation file from freesurfer space to native space
        """

        for surfName in surfNames:
            aux = surfName.replace('_','.')
            vtkToConvert = os.path.join(dir, aux+".vtk")
            ascSurf = os.path.join(dir, surfName+".asc")
            if os.path.exists(vtkToConvert):
                self.MRIsConvert(in_file=vtkToConvert, out_file=ascSurf, out_datatype='vtk')

    def createSegmentationClosedSurfaceRepresantationFromLabelMap(self):
        # Import libraries
        segs = []

        for segName in self.segs:

            labelMapPath = os.path.join(self.finalSegDir, self.segs[segName][0])
            vtmPath = os.path.join(self.finalSegDir, self.segs[segName][1])

            colorNode = slicer.util.loadColorTable(os.path.join(moduleDir, "FreeSurferLabels.ctbl"))
            labelMapNode = slicer.util.loadVolume(labelMapPath, {"labelmap": True,'colorNodeID': colorNode.GetID()})

            seg = slicer.mrmlScene.AddNewNodeByClass("vtkMRMLSegmentationNode")

            slicer.modules.segmentations.logic().ImportLabelmapToSegmentationNode(labelMapNode, seg)

            seg.CreateClosedSurfaceRepresentation()
            seg.SetName(segName)
            segmentation = seg.GetSegmentation()
            segmentation.SetMasterRepresentationName(vtkSegmentationCore.vtkSegmentationConverter.GetSegmentationClosedSurfaceRepresentationName())
            seg.CreateDefaultDisplayNodes()
            slicer.util.saveNode(seg, vtmPath)

            segs.append(seg)
        
        return segs





        
    def runConverts(self):
        

        self.convertAsegToNative()
        self.convertAparcPlusAsegToNative()
        self.convertAparc2009PlusASegToNative()


        self.exportPialAndWhiteFromFsSurf()
        self.exportFreesurferAsegLabelMap()
        self.exportFreesurferAparcPlusASegLabelMap() 
        self.exportFreesurferAparc2009PlusASegLabelMap()


        return self.createSegmentationClosedSurfaceRepresantationFromLabelMap()
        