import slicer, os, logging, re, vtk, shutil

SEGMENTATIONS_OPACITY_2D_FILL = 0.3


def getString(stringPath):

    buf = open(stringPath,'r').readlines()
    return buf[0]

def runCommand(command):
    param = {
        "command": command
    }

    cliNodeDownload = slicer.cli.runSync(slicer.modules.backgroundprocess, parameters=param)

    if cliNodeDownload.GetStatus() & cliNodeDownload.ErrorsMask:
        # error
        errorText = cliNodeDownload.GetErrorText()
        slicer.mrmlScene.RemoveNode(cliNodeDownload)
        logging.error('Failed to run command: '+ command)
        raise ValueError("CLI execution failed: " + errorText)


class AtlasYEBPipeline:
    def __init__(self, subjectName, LUTPath, terminologyPath):
        self.LUTPath = LUTPath
        self.terminologyPath = terminologyPath
        self.yebLHSegmentationNode = slicer.vtkMRMLSegmentationNode()
        self.yebRHSegmentationNode = slicer.vtkMRMLSegmentationNode()
        self.subjectName = subjectName
    
    def runAtlasYEB(self, inputVolume):

        atlasYebSegmentationNameLeft = 'Atlas YeB models - left'
        atlasYebSegmentationNameRight = 'Atlas YeB models - right'

        if not inputVolume:
            raise ValueError("Input or output volume is invalid")

        # Create new empty folder
        tempFolder = slicer.util.tempDirectory()
        outputAtlasYEBFolder = tempFolder + "/atlasYEB_register"
        inputFile = outputAtlasYEBFolder+"/mri.nii.gz" 
        imageName = "mri.nii.gz"
      
        yebScenePath = outputAtlasYEBFolder + "/slicer_scene/slicer_scene_blockmatching_linear_reg.mrml"
        yebLUT = outputAtlasYEBFolder + "/slicer_scene/fantomas_color_LUT.ctbl"
        aux=os.path.dirname(self.LUTPath)
        oldStringColorPath=os.path.join(aux,'oldColorTableString.txt')
        newStringColorPath=os.path.join(aux,'newColorTableString.txt')

        oldColorString=getString(oldStringColorPath)
        newColorString=getString(newStringColorPath)

        import time
        startTime = time.time()
        logging.info('Processing started')


        volumeStorageNode = slicer.mrmlScene.CreateNodeByClass("vtkMRMLVolumeArchetypeStorageNode")
        volumeStorageNode.SetFileName(inputFile)
        volumeStorageNode.UseCompressionOff()
        volumeStorageNode.WriteData(inputVolume)
        volumeStorageNode.UnRegister(None)

        dockerCommand = 'docker run --rm --user $(id -u):$(id -g)' \
            + ' -e "HOME=/home/user/"'\
            + ' -v {0}:{1} registry.gitlab.com/icm-institute/stim/atlasyeb/atlasyeb:v4 AtlasYEBRegistration.py'\
            + ' -niftipath {1}/{2}'\
            + ' -workdir {1}'\
            + ' -reg {3} & sleep 10s && /usr/local/bin/docker stop --time 600 $(/usr/local/bin/docker ps -a -q -l) '
        
        dockerCommand = dockerCommand.format(
            outputAtlasYEBFolder,
            '/home/user',
            imageName,
            'blockmatching'
        )

        runCommand(dockerCommand)

        

        # if the scene is generate, the process worked 
        #      we need to change  the fantomas lut path
        #      also copy the lut on the slicer_scene path

        import shutil
        if os.path.exists(yebScenePath):
            shutil.copyfile(self.LUTPath,yebLUT)

            with open(yebScenePath, "rt") as file:
                x = file.read()
            with open(yebScenePath, "rt") as file:
                x = x.replace(oldColorString,'<!-- nothing -->')
                x = x.replace("../../../cenir_software/AtlasYEBRegistration/template/yeb_bgatlas/fantomas_color_LUT.ctbl","./fantomas_color_LUT.ctbl")
            fin = open(yebScenePath, "wt")
            fin.write(x)
            fin.close()
            
                    
        slicer.util.loadScene(yebScenePath)
        
        self.updateDisplayNodesYEB()

            
        

        self.yebLHSegmentationNode.SetName(atlasYebSegmentationNameLeft)
        self.yebRHSegmentationNode.SetName(atlasYebSegmentationNameRight)
        slicer.mrmlScene.AddNode(self.yebLHSegmentationNode)
        slicer.mrmlScene.AddNode(self.yebRHSegmentationNode)
        self.yebLHSegmentationNode.CreateDefaultDisplayNodes()
        self.yebRHSegmentationNode.CreateDefaultDisplayNodes()


        self.importModelsToSegmentations()

        """ tableLeft.setSegmentationNode(self.yebLHSegmentationNode)
        tableRight.setSegmentationNode(self.yebRHSegmentationNode) """


        nodes = self.getSceneMeshes()
        DMLogic = slicer.modules.DataManagementWidget.logic
        for node in nodes:
            node.SetName(self.subjectName + "_" + re.sub(r'.vtk', '', node.GetName()))
            DMLogic.registerNode(node, ["mesh", "ref_t1mri", "yeb_atlas"], node.GetName() + ".vtk")

        shutil.copy(
            os.path.join(outputAtlasYEBFolder, "slicer_scene", "fantomas_color_LUT.ctbl"), 
            os.path.join(DMLogic.subject.path, "mesh", "ref_t1mri", "yeb_atlas", DMLogic.subject.folderName + "_fantomas_color_LUT.ctbl")
            )

        if(not os.path.exists(os.path.join(DMLogic.subject.regMatDir, "ref_t1mri"))):
            os.makedirs(os.path.join(DMLogic.subject.regMatDir, "ref_t1mri"))
        with os.scandir(os.path.join(outputAtlasYEBFolder, "reg_mat")) as dirContent:
            for fObject in dirContent:
                if(fObject.is_file()):
                    shutil.copy(fObject.path, os.path.join(DMLogic.subject.regMatDir, "ref_t1mri", DMLogic.subject.folderName + "_" + fObject.name))

        stopTime = time.time()

    def updateDisplayNodesYEB(self):

        # update visibility of inclusion meshes
        listOfMeshes = self.getNamesFromTerminoly()

        for stru in listOfMeshes :
            print(stru)
            displayNodeID = self.getMeshYebAtlasDisplayNodeId(stru, "LEFT")
            displayNodeID = self.getMeshYebAtlasDisplayNodeId(stru, "RIGHT")
            if listOfMeshes[stru]['show'] == 'True':
                print(listOfMeshes[stru])
                self.setNodeVisibility(displayNodeID, 1)
            else :
                self.setNodeVisibility(displayNodeID, 0)
            
    def setNodeVisibility(self, displayNodeID, visibility, sliceVisiblity=None):

        if sliceVisiblity is None:
            sliceVisiblity = visibility
        if displayNodeID is not None:
            print(displayNodeID)
            node =  slicer.mrmlScene.GetNodeByID(displayNodeID)
            if node is not None:
                node.SetVisibility(visibility)
                if hasattr(node,'SetSliceIntersectionVisibility'):
                    node.SetVisibility2D(sliceVisiblity)
            else:
                pass
        else:
            pass    
    
    def getNamesFromTerminoly(self):

        from xml.dom import minidom
        liste={}

        dom = minidom.parse(self.terminologyPath)
        for node in dom.getElementsByTagName('nucleus') :
            [name,label,show,description,partOf] = self.getNucleusInfoFromXMLNode(node)
            liste[name] = {'label':label, 'description':description, 'show':show, 'partOf': partOf}
        return liste
    
    def getNucleusInfoFromXMLNode(self, node):
        name=str(node.getAttribute('name'))
        label=str(node.getAttribute('label'))
        show=str(node.getAttribute('show'))
        description=str(node.getAttribute('description'))
        partOf=str(node.getAttribute('partOf'))
        return [name,label,show,description,partOf]

    def getMeshYebAtlasDisplayNodeId(self,meshName,laterality):

        h= "RH_"
        if laterality == "LEFT" :
            h= "LH_"
        return 'Linear_'+h+meshName+'-ON-pmMR.MR-Geometry_NIIWorld.vtk_display'





    def importModelsToSegmentations(self):
        meshesNames = self.getNamesFromTerminoly()

        modelsLeft  = []
        modelsRight = []
        for meshName in meshesNames:
            print(meshName)
            # Left meshes
            modelNodeId = self.getInclusionMeshYebAtlasNodeId(meshName, 'LEFT')
            modelNode = slicer.mrmlScene.GetNodeByID(modelNodeId)
            modelDescription = self.getInclusionMeshYebAtlasDescription(meshName)
            if modelNode:
                modelsLeft.append((modelNode, modelDescription))

            # Right meshes
            modelNodeId = self.getInclusionMeshYebAtlasNodeId(meshName, 'RIGHT')

            modelNode = slicer.mrmlScene.GetNodeByID(modelNodeId)
            modelDescription = self.getInclusionMeshYebAtlasDescription(meshName)
            if modelNode:
                modelsRight.append((modelNode, modelDescription))

        self.importModelsToSegmentationNode(modelsLeft, self.yebLHSegmentationNode)
        self.importModelsToSegmentationNode(modelsRight, self.yebRHSegmentationNode)
    
    def importModelsToSegmentationNode(self, models, segmentationNode=None):
        """
        Models is a list of tuples: (modelPath, segmentName)
        """
        import vtkSegmentationCorePython

        segmentation = segmentationNode.GetSegmentation()
        segmentation.SetSourceRepresentationName(vtkSegmentationCorePython.vtkSegmentationConverter.GetSegmentationClosedSurfaceRepresentationName())
        for modelNode, modelDescription in models:
            modelDisplayNode = modelNode.GetDisplayNode()
            modelVisibility = modelDisplayNode.GetVisibility()
            modelDisplayNode.SetVisibility(False)
            segment = slicer.vtkSlicerSegmentationsModuleLogic.CreateSegmentFromModelNode(modelNode)
            if modelDescription:
                segment.SetName(modelDescription)
            segmentation.AddSegment(segment)
            segmentID = segmentation.GetSegmentIdBySegment(segment)
            segmentationDisplayNode = segmentationNode.GetDisplayNode()
            segmentationDisplayNode.SetSegmentVisibility(segmentID, modelVisibility)
            segmentationNode.GetDisplayNode().SetOpacity2DFill(SEGMENTATIONS_OPACITY_2D_FILL)

    def getInclusionMeshYebAtlasNodeId(self,meshName,laterality):

        h= "RH_"
        if laterality == "LEFT" :
            h= "LH_"
        return 'Linear_'+h+meshName+'-ON-pmMR.MR-Geometry_NIIWorld.vtk'

    def getInclusionMeshYebAtlasDescription(self,meshName):


        listeOfDiseaseMeshes=self.getNamesFromTerminoly()
        inclusionSidedMeshDescriptionMapYEB = {}
        for stru in listeOfDiseaseMeshes :
            inclusionSidedMeshDescriptionMapYEB[stru] = listeOfDiseaseMeshes[stru]['description']

        return inclusionSidedMeshDescriptionMapYEB[meshName]

    def getNamesFromTerminoly(self):

        def getNucleusInfoFromXMLNode(node):
            name=str(node.getAttribute('name'))
            label=str(node.getAttribute('label'))
            show=str(node.getAttribute('show'))
            description=str(node.getAttribute('description'))
            partOf=str(node.getAttribute('partOf'))
            return [name,label,show,description,partOf]



        from xml.dom import minidom
        liste={}

        dom = minidom.parse(self.terminologyPath)
        for node in dom.getElementsByTagName('nucleus') :
            [name,label,show,description,partOf] = getNucleusInfoFromXMLNode(node)
            liste[name] = {'label':label, 'description':description, 'show':show, 'partOf': partOf}
        return liste

    def getSceneMeshes(self):
        result = []
        shn = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
        childrenNodesIDs = vtk.vtkIdList()
        shn.GetItemChildren(shn.GetSceneItemID(), childrenNodesIDs, False)
        for i in range(childrenNodesIDs.GetNumberOfIds()):
            childNodeID = childrenNodesIDs.GetId(i)
            nodeName = shn.GetItemName(childNodeID)
            if(".vtk" in nodeName):
                result.append(shn.GetItemDataNode(childNodeID))
        return result